package rockPaperScissors;
import java.util.Random;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        int quit=0;

while (quit<1)  {
    System.out.println("Let's play round " + roundCounter);
    String human = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
    while (true) {
    if (human.equals("rock") || human.equals("paper") ||  human.equals("scissors"))   { break;}
    else{System.out.println ("I don't understand " + human + ". try again!");
    human = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();} }

    String computer = rpsChoices.get(new Random().nextInt(rpsChoices.size()));
    String result = ("Human chose " + human + ". computer chose " + computer);

    if (human.equals(computer)){ System.out.println(result + " It's a tie!");}

    //human wins
    else if ((human.equals("rock") && computer.equals("scissors")) || 
    (human.equals("scissors") && computer.equals("paper")) ||
    (human.equals("paper") && computer.equals("rock"))){
        System.out.println(result + " human wins.");
        humanScore +=1; }

    else {
        System.out.println(result + " computer wins.");
        computerScore +=1;}
    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
    
    roundCounter +=1;
    while (true){
    String playagain = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
    if (playagain.equals("n")) {
        System.out.println("Bye bye :)");
        quit +=1;
        break;}
    else if (playagain.equals("y")){ break;}
    else {System.out.println("I do not understand Could you try again?");}
    
    }}

    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
